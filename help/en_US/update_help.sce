// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.


///////////////////////////////////////////////////////////////////////////
//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating specfun\n");
helpdir = cwd;
funmat = [
  "specfun_lambertw"
  "specfun_gammainc"
  "specfun_log1p"
  "specfun_expm1"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "specfun";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the Discrete Maths help
mprintf("Updating specfun/discrete\n");
helpdir = fullfile(cwd,"discrete");
funmat = [
  "specfun_factorial"
  "specfun_factoriallog"
  "specfun_nchoosek"
  "specfun_pascal"
  "specfun_combine"
  "specfun_combinerepeat"
  "specfun_ismember"
  "specfun_subset"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "specfun";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

